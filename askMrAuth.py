##
## The server and client communicate by sending json strings
## Etienne de Villiers (2019)
##

import socket
import json
import random
import time
import threading
import argparse, sys

#Thread for server connection, handles incoming json objects and performs actions
class ServerThread(threading.Thread):
    def __init__(self, client=None):
        threading.Thread.__init__(self)
        self.client = client
        self.s = self.client.s

        self.joinableRoomCallback = None
        self.join_room = None
        

    def run(self):
        while self.client.running: # main loop for serverthread
            try:
                data = self.s.recv(1024)
            except:
                break
            if not data: break #connection closed/broken
            

            #Sometimes more than one json objects are recieved
            #This decodes the multiple json objects into a list commands
            data = data.decode('utf-8').split('}')
            commands = []
            for dat in data:
                if len(dat) > 1:
                    commands.append(dat+'}')
           
            for command in commands:
                try:
                    command = json.loads(command)
                except:
                    print("Garbage command: ", command)
                else:
                    if command["action"] == "done": # Infromation of a joinable room
                        self.client.exitCode = int(command["exitCode"])
                        self.client.running = False
                        print(command["message"])
                    
        self.client.running = False

## Client object, send server commands
class Client():
    def __init__(self, parent=None, ip="127.0.0.1", port=25565):
        self.HOST = ip
        self.PORT = port
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.running = True
        self.serverthread = None

        self.exitCode = 2

    #close connections, this causes the serverthread to crash. good enough
    def stop(self):
        self.s.close()

    #Make connection to server
    def connect(self):
        connected = False
        try:
            self.s.connect((self.HOST, self.PORT))
            connected = True
        except: #port is closed/broken, try the next one. this is not the way to handle this problem
            print("Server error")
            exit(2)

        self.serverthread = ServerThread(client=self)
        self.serverthread.start()

    def sendMessage(self, email="", message="Message Contents"):
        command = {
                "action" : "sendTelegramMessage",
                "email" : email,
                "message": message
        }
        data = json.dumps(command)
        self.s.sendall(data.encode('utf-8')) #send data
    
    def waitToFinish(self):
        while self.running:
            pass
        self.stop()
        self.serverthread.join()
        exit(self.exitCode)



parser = argparse.ArgumentParser(description='This script interfaces with the MrAuthenticator bot')
parser.add_argument("-e", help="Email of user to be notified/asked", metavar='user_name')
parser.add_argument("-m", help="Message that user will recieve", metavar='message', default="Did you make a login request?")
parser.add_argument('-n', help='Send simple message', action='store_true')
parser.add_argument("-i", help="IP of machine bot is located on", metavar='ip', default="127.0.0.1")
parser.add_argument("-p", help="Port for bot querries", metavar='port', default="25565")
args = parser.parse_args()

if args.n:
    if args.m and args.e:
        client = Client(ip=args.i, port=int(args.p))
        client.connect()
        client.sendMessage(email=args.e, message=args.m)
        client.waitToFinish()
        client.stop()
    else:
        print("please specify email end message")
elif not ('-h' in sys.argv):
        print('Use -h for all options.')
    
