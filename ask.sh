#!/bin/bash

cd  /usr/local/mrauthEnv
. bin/activate

IP=`echo $SSH_CONNECTION|awk {'print $1'}`
HOST=`hostname`
MESSAGE="User ${USER}@${HOST} just logged in from $IP"
email=`grep $USER /etc/aliases|awk {'print $NF'}|tail -1`


if [ "${email:-notset}" != "notset" ] ; then  
        python3 mrauthenticator/askMrAuth.py -n -e ${email} -m "${MESSAGE}" > /dev/null
        if [ $? -eq 2 ] ; then
            tput blink
            echo " /-------------------------------------------------\ "
            echo " | Please chat with telegram bot @APPSolveAuthBot  | "
            echo " |           and follow instructions               | "
            echo " \-------------------------------------------------/ "
            echo
            tput sgr0
        fi
fi