'''

Commandline telegram bot fot authenticating Users
By Etienne de Villiers

'''

import telegram
from telegram.error import NetworkError, Unauthorized
from time import time, sleep
import datetime
import json
import argparse
import sys, argparse
from threading import Timer
import smtplib, ssl
from random import randrange
import socket
import threading
import re
import telegram.vendor.ptb_urllib3.urllib3 as urllib3
import os




# /help message
HELPMESSAGE = """\
/start - Check if the bot knows who you are
/help  - Bring up this help message
"""


#Function to verify email
def isValidEmail(email):
    pattern = '.*@appsolve.co.za'
    if re.match(pattern, email):
        print(email)
        return True
    if email.lower() == "etiennefdevilliers@gmail.com":
        return True
    return False


#load program settings
with open('mrauthconfig.json') as json_file:
    data = json.load(json_file)
    key = data["key"]
    verificationEmail = data["email"]
    verificationEmailPassword = data["password"]

    proxy_url =  data["proxy_url"]
    urllib3_proxy_kwargs =  {
            'proxy_headers': urllib3.make_headers(proxy_basic_auth=data["proxy_username"] + ":" + data["proxy_password"])
    }

#Function to send verification email
message = """\
Subject: Your AppSolveAuth code
"""
def sendVerificationMail(receiver_email, code):
    global verificationEmail
    global verificationEmailPassword
    global message

    message = "To: " + receiver_email + "\n"
    message += "Subject: Your AppSolveAuth code\n"
    message += "\n"
    message += str(code)

    smtp_server = "smtp.gmail.com"
    port = 587  # For starttls
    #context = ssl.create_default_context()
    context = ssl._create_unverified_context()
    with smtplib.SMTP(smtp_server, port) as server:
        server.ehlo()  # Can be omitted
        server.starttls(context=context)
        server.ehlo()  # Can be omitted
        server.login(verificationEmail, verificationEmailPassword)
        server.sendmail(verificationEmail, receiver_email, message)

btnYes = telegram.KeyboardButton(text="Yes")
btnNo = telegram.KeyboardButton(text="No")
custom_keyboard = [[ btnNo, btnYes ]]
reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard)


class ClientThread(threading.Thread):
    def __init__(self, parent):
        threading.Thread.__init__(self)
        self.parent = parent 
        self.parent.clients.append(self) #Add self to the list of client threads
        self.connected = False # Does this thread have a client connected

    def run(self):
        try:
            self.conn, self.addr = self.parent.s.accept() #accept incoming connection
        except ConnectionAbortedError:
            return
        self.connected = True #set state to connected
        self.parent.newCThread() #spawn new ClientThread to wait for new connection

        while not self.parent.done:
            try:
                data = self.conn.recv(1024) #Recieve data
            except:
                break #connection closed
            if not data: break
            #print(self, data)

            #Sometimes more than one json objects are recieved
            #This decodes the multiple json objects into a list commands
            data = data.decode('utf-8').split('}') 
            commands = []
            for dat in data:
                if len(dat) > 1:
                    commands.append(dat+'}')
           
            for command in commands:
                try:
                    command = json.loads(command)
                except:
                    print("Garbage command: ", command) #Something went wrong. Maby because only half a json object got recieved
                else:
                    if command["action"] == "sendTelegramMessage": # Command for creating and joining room
                        if self.parent.sendTelegramMessage(email=command["email"], message=command["message"]):
                            command = {
                                    "action" : "done",
                                    "exitCode" : "0",
                                    "message":"message sent"
                            }
                            data = json.dumps(command).encode('utf-8')
                            self.send(data)
                        else:
                            command = {
                                    "action" : "done",
                                    "exitCode" : "2",
                                    "message":"user doesn't exist"
                            }
                            data = json.dumps(command).encode('utf-8')
                            self.send(data)

                            self.close()    
        self.close()



    def close(self):
        #Client has disconnected or a connection error has occured
        #Remove and destroy thread

        try:
            self.conn.close()
        except:
            pass

        self.connected = False 
        self.parent.clients.remove(self)


    # Function for server class to send data to client
    def send(self, data):
        self.conn.send(data)





class MrAuth:
    def __init__(self, toAuth=None, toAuthMessage="New login, is this you?", timerTime=30, host="", port=25565):
        self.cThreads = [] # Each client has a thread, this is to keep track of them
        self.clients = [] # List of incoming connection

        self.HOST = host
        self.PORT = int(port)

        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        print("starting querries on ", self.HOST, ":", self.PORT)
        try:
            self.s.bind((self.HOST, self.PORT))
        except Exception as e:
            print("Port already in use")
            exit(2)
        self.s.listen(128)


        if proxy_url == "http://PROXY_HOST:PROXY_PORT/":
            self.bot = telegram.Bot(key)
        else:
            pp = telegram.utils.request.Request(proxy_url=proxy_url, urllib3_proxy_kwargs=urllib3_proxy_kwargs)
            self.bot = telegram.Bot(key, request=pp)

        self.toAuth = toAuth

        self.done = False

        self.usersAwaitingEmails = []
        self.usersAwaitingCodes = {}


        # get the first pending update_id, this is so we can skip over it in case
        # we get an "Unauthorized" exception.
        try:
            self.update_id = self.bot.get_updates()[0].update_id
        except IndexError:
            self.update_id = None
        
        self.commands = {'/start':self.startNewUser, '/help':self.sendHelp}
        

        with open('userDict.json') as json_file:
            self.userDict = json.load(json_file)
        
        
        

        self.newCThread()
    
    def newCThread(self):
            nct = ClientThread(self)
            nct.start()
            self.cThreads.append(nct)
    


    def loop(self):
        while not self.done:
            try:
                for update in self.bot.get_updates(offset=self.update_id, timeout=10):
                    self.update_id = update.update_id + 1
                    if update.message:
                        self.processNewMessage(update)
                            
            except NetworkError:
                sleep(1)
            except Unauthorized:
                # The user has removed or blocked the bot.
                update_id += 1
            except KeyboardInterrupt:
                print("Bot shutting down, reason: KeyboardInterrupt")
                break
        self.s.close()
        #print("Waiting for threads to join...")
        self.done = True
        

        os._exit(0)
        for thrd in self.cThreads: ## wierd behaviour on linux
            thrd.join()
    
    def sendTelegramMessage(self, email=None, message="sendTelegramMessage"):
        if email in self.userDict:
            chat_id = self.userDict[email]["chat_id"]
            self.bot.send_message(chat_id=chat_id, text=message)
            return True
        else:
            return False
    
    def processNewMessage(self,update):
        if update.effective_user.name in self.usersAwaitingEmails:
            self.checkEmail(update)
        elif update.effective_user.name in self.usersAwaitingCodes:
            self.checkCode(update)
        elif update.message.text in self.commands:
            self.commands[update.message.text](update)
        else:
            update.message.reply_text("I'm a simple bot. boop beep bop")

    
    def checkEmail(self,update):
        name = update.effective_user.name
        email = update.message.text
        if isValidEmail(email) and (not (email in self.userDict)):
            verificationCode = randrange(0,9999)
            try:
                update.message.reply_text("Sending email with verification code now...")
                sendVerificationMail(email, verificationCode)
            except Exception as e:
                update.message.reply_text("I am unable to send an email. Please contact my administrator")
                print("Email send error",e)
            else:
                update.message.reply_text("Email sent, please check your inbox")
                self.usersAwaitingEmails.remove(name)
                self.usersAwaitingCodes[name] = {"code":verificationCode, "email":update.message.text}
        else:
            update.message.reply_text("Sending email with verification code now....")
            update.message.reply_text("Email sent, please check your inbox")
            self.usersAwaitingEmails.remove(name)
    

    def checkCode(self, update):
        name = update.effective_user.name
        if name in self.usersAwaitingCodes:
            if str(self.usersAwaitingCodes[name]["code"]) == update.message.text:
                email = self.usersAwaitingCodes[name]["email"].lower()

                self.userDict[email] = {"telegram_user_name":name, "chat_id":update.effective_chat.id}
                with open('userDict.json', 'w') as outfile:
                    json.dump(self.userDict, outfile) 

                self.usersAwaitingCodes.pop(name)

                update.message.reply_text("Great, I am confident you are who you say you are.")
            else:
                update.message.reply_text("Code incorrect, please submit /start again")
                self.usersAwaitingCodes.pop(name)
        else:
            update.message.reply_text("I'm not sure what I'm supposed to do now. Please try again")
    
    def startNewUser(self, update): 
        #if update.effective_user.username in self.userDict:
        name = update.effective_user.name

        for user in self.userDict:
            if self.userDict[user]["chat_id"] == update.effective_chat.id:
                update.message.reply_text("I am confident you are who you say you are.")
                return

        update.message.reply_text("Hello Stranger. I need to verify your identity. Please share with me your full appsolve email address.")
        self.usersAwaitingEmails.append(name)
    
    def sendHelp(self, update):
        for user in self.userDict:
            if self.userDict[user]["chat_id"] == update.effective_chat.id:
                update.message.reply_text(HELPMESSAGE)
                return

    

    
parser = argparse.ArgumentParser(description='Bot that sends notifications to users')
parser.add_argument('-l', help='Only allow querries from local host', action='store_true')
parser.add_argument("-p", help="Port for bot querries", metavar='port', default="25565")

args = parser.parse_args()
    

if __name__ == "__main__":
    if args.l:
        print("Only allowing querries from local host")
        MrAuth(host='127.0.0.1', port=args.p).loop()
    else:
        MrAuth(port=args.p).loop()
        

